
(* Timer function which generates a pulse *)
FUNCTION_BLOCK TPC
	
	IF IN THEN
		IF NOT(IS.oldEnable) OR ET >= PT THEN
			ET				:= 0;
			IS.startTime	:= clock_ms();
		END_IF
		
		// set enabled outputs
		IS.currentTime	:= clock_ms();
		ET				:= MIN((IS.currentTime - IS.startTime), PT);
	ELSE // function is disabled, act as reset
		ET	:= 0;
		Q	:= FALSE;
	END_IF
	
	// set outputs
	Q	:= ET >= (PT / 2);
	IS.oldEnable	:= IN;
	
END_FUNCTION_BLOCK



(* Retentive Time ON function block *)
FUNCTION_BLOCK TONR
	
	// accept reset command regardless of IN
	IF RES AND NOT(IS.oldReset) THEN
		ET	:= 0;
	END_IF
	
	IF IN THEN
		IF NOT(IS.oldEnable) THEN
			IS.prevTime	:= clock_ms();
		ELSE
			IS.prevTime	:= IS.currentTime;
		END_IF
		
		// set enabled outputs
		IS.currentTime	:= clock_ms();
		ET				:= MIN(ET + (IS.currentTime - IS.prevTime), PT);
	END_IF
		
	// set outputs
	Q				:= ET >= PT;
	IS.oldEnable	:= IN;
	IS.oldReset		:= RES;
	
END_FUNCTION_BLOCK



(* Retentive Time ON function block with option to retain elapsed time *)
FUNCTION_BLOCK TONRP
	
	inET ACCESS pET;
	
	// accept reset command regardless of IN
	IF RES AND NOT(IS.oldReset) THEN
		inET	:= 0;
	END_IF
	
	IF IN THEN
		IF NOT(IS.oldEnable) THEN
			IS.prevTime	:= clock_ms();
		ELSE
			IS.prevTime	:= IS.currentTime;
		END_IF
		
		// set enabled outputs
		IS.currentTime	:= clock_ms();
		inET			:= MIN(inET + (IS.currentTime - IS.prevTime), PT);
	END_IF
		
	// set outputs
	ET				:= inET;
	Q				:= inET >= PT;
	IS.oldEnable	:= IN;
	IS.oldReset		:= RES;
	
END_FUNCTION_BLOCK



(*Time on function with intuitive count-down display and flexible input time*)
FUNCTION_BLOCK TOFU
	
	IF IN THEN
		// calculate length of scan
		IF NOT IS.oldEnable THEN
			IS.prevTime	:= clock_ms();
		ELSE
			IS.prevTime		:= IS.currentTime;
		END_IF
		// update current time
		IS.currentTime	:= clock_ms();
		
		// accept user input
		IS.displayTime	:= TIMEStructure_TO_TIME(pTS);
		IF IS.displayTime <> IS.internalTime THEN
			IS.internalTime	:= IS.displayTime;
		END_IF
		
		IF RES AND NOT IS.oldReset THEN
			IS.internalTime	:= 0;
		END_IF
		
		// pass time
		IF IS.internalTime <> 0 AND IN THEN
			IS.internalTime	:= IS.internalTime - (IS.currentTime - IS.prevTime);
		END_IF
		
		IF IS.displayTime <> IS.internalTime THEN
			IS.displayTime	:= IS.internalTime;
			TIME_TO_TIMEStructure(IS.displayTime, pTS);
		END_IF	
	END_IF
	
	IS.oldEnable	:= IN;
	IS.oldReset		:= RES;
	Q				:= (IS.internalTime <> 0);
	
END_FUNCTION_BLOCK


(********************************************************
 * COPYRIGHT: B&R Industrial Automation
 ********************************************************
 * Author:	Brad Jones
 * Created:	September 16, 2016/8:39 AM 
 ********************************************************
 [program information]:
 V1.0 - keeps track of time a machine spends in a condition
        without using timers. Requires permanent variable.
        Output format is configurable using MR_* constants.
 V1.1 - Added version number to status string of after good
		init.
 V2.0 - Modified I/O on fub to comply with library design
		guidelines, standard interface.
		Removed Info str output to cut down on unneccessary
		comp time. Replaced with enumerated StatusID.
 V3.0 - Removed the cycle time parameter, should be able
		to run regardless of task class. Using clock_ms().
		Added reset command to reset the counter value.
 V3.1 - Fixed bug that would add lots of time on first scan
		due to prevClock value not being populated.
 ********************************************************)

(* Keeps time that machine is in a certain state *)
FUNCTION_BLOCK TRUN		
	(*Errors are > 10000*)
	(*Warnings are < 10000*)
	(*No messages = 0*)	
	
	IF Enable THEN
		// only operate function block if parameters are valid
		IF NOT(Active) THEN // PRE-OPERAION
			brsmemset(ADR(Runtime), 0, SIZEOF(Runtime));
				
			// check for valid parameters
			IF TimeUnit > 5 THEN
				StatusID		:= TIME_ERR_TRUN_LARGE_UNIT_INVALID;
				Error			:= TRUE;
			ELSIF ADR(pTime) = 0 THEN
				StatusID		:= TIME_ERR_TRUN_NULL_POINTER;
				Error			:= TRUE;
			ELSE // no errors, proceed to MAIN OPERATION			
				IF pTime = 0 THEN 
					StatusID	:= TIME_WARN_TRUN_STARTED_FROM_ZERO;
				ELSE
					StatusID	:= TIME_INFO_FUB_OK;
				END_IF
			
				Error			:= FALSE;
				IS.useTimeUnit	:= TimeUnit;
				Active			:= TRUE;
				IS.VERSION		:= 'V3.0';
				IS.clockms		:= TIME_TO_UDINT(clock_ms());
			END_IF
		ELSE // MAIN OPERATION
			// count up in ms
			IS.old_clockms		:= IS.clockms;
			IS.clockms			:= TIME_TO_UDINT(clock_ms());
			
			IF IN THEN
				IS.internalMs	:= IS.internalMs + (IS.clockms - IS.old_clockms);
				
				// generate seconds from the accumulated ms
				WHILE IS.internalMs > 1000 DO
					IS.internalMs	:= IS.internalMs - 1000; // simulate MOD
					pTime			:= pTime + 1;
				END_WHILE	
			END_IF
			
			// check for updates in parameters
			IF (Update AND NOT(IS.old_update)) THEN
				Active		:= FALSE;
			ELSIF (RES AND NOT(IS.old_reset)) THEN
				Active		:= FALSE;
				pTime		:= 0;
			END_IF
					
			// assign runtime values - only do the operations necessary
			// also only update on even second ticks
			IF (IS.clockms MOD 1000) < (IS.old_clockms MOD 1000) THEN
				CASE IS.useTimeUnit OF
					brusTRUN_SECONDS:
						Runtime.seconds	:= pTime;
				
					brusTRUN_MINUTES:
						Runtime.seconds	:= pTime MOD 60;
						Runtime.minutes	:= pTime / brustimersSEC_IN_MIN;
				
					brusTRUN_HOURS:
						Runtime.seconds	:= pTime MOD 60;
						Runtime.minutes	:= (pTime / brustimersSEC_IN_MIN) MOD 60;
						Runtime.hours	:= (pTime / brustimersSEC_IN_HOUR);
				
					brusTRUN_DAYS:
						Runtime.seconds	:= pTime MOD 60;
						Runtime.minutes	:= (pTime / brustimersSEC_IN_MIN) MOD 60;
						Runtime.hours	:= (pTime / brustimersSEC_IN_HOUR) MOD 24;
						Runtime.days	:= (pTime / brustimersSEC_IN_DAY);
				
					brusTRUN_WEEKS:
						Runtime.seconds	:= pTime MOD 60;
						Runtime.minutes	:= (pTime / brustimersSEC_IN_MIN) MOD 60;
						Runtime.hours	:= (pTime / brustimersSEC_IN_HOUR) MOD 24;
						Runtime.days	:= (pTime / brustimersSEC_IN_DAY) MOD 7;
						Runtime.weeks	:= (pTime / brustimersSEC_IN_WEEK);
				
					brusTRUN_YEARS:
						Runtime.seconds	:= pTime MOD 60;
						Runtime.minutes	:= (pTime / brustimersSEC_IN_MIN) MOD 60;
						Runtime.hours	:= (pTime / brustimersSEC_IN_HOUR) MOD 24;
						Runtime.days	:= (pTime / brustimersSEC_IN_DAY) MOD 365;
						Runtime.years	:= (pTime / brustimersSEC_IN_YEAR);
					ELSE // parameter out of range
						Active	:= FALSE; // send back to init
				END_CASE
			END_IF
			
			IS.old_update	:= Update;
			IS.old_reset	:= RES;
		END_IF
	ELSE
		StatusID	:= TIME_ERR_FUB_DISABLED;
		Active		:= FALSE;
		Error		:= FALSE;
		brsmemset(ADR(Runtime), 0, SIZEOF(Runtime));
	END_IF
	
END_FUNCTION_BLOCK