
TYPE
	IS_AdvTimers : 	STRUCT 
		oldEnable : BOOL;
		oldReset : BOOL;
		startTime : TIME;
		prevTime : TIME;
		currentTime : TIME;
		internalTime : TIME;
		displayTime : TIME;
	END_STRUCT;
END_TYPE

(*MR - machine runtime structures*)

TYPE
	BRUS_TRUN_TimeUnit_enum : 
		(
		brusTRUN_YEARS := 5,
		brusTRUN_WEEKS := 4,
		brusTRUN_DAYS := 3,
		brusTRUN_HOURS := 2,
		brusTRUN_MINUTES := 1,
		brusTRUN_SECONDS := 0
		);
	BRUS_TRUN_Times_typ : 	STRUCT 
		years : UDINT;
		weeks : UDINT;
		days : UDINT;
		hours : UDINT;
		minutes : UDINT;
		seconds : UDINT;
	END_STRUCT;
	BRUS_TRUN_IS : 	STRUCT 
		internalMs : UDINT;
		clockms : UDINT;
		old_clockms : UDINT;
		useTimeUnit : BRUS_TRUN_TimeUnit_enum;
		old_reset : BOOL;
		old_update : BOOL;
		udint_zero : UDINT;
		VERSION : STRING[10];
	END_STRUCT;
	BRUS_TRUN_ID_enum : 
		(
		TIME_INFO_FUB_OK := 0,
		TIME_WARN_TRUN_STARTED_FROM_ZERO := 9999,
		TIME_ERR_TRUN_CYCLE_TIME_INVALID := 10001,
		TIME_ERR_TRUN_LARGE_UNIT_INVALID := 10002,
		TIME_ERR_TRUN_NULL_POINTER := 10003,
		TIME_ERR_FUB_DISABLED := 65534
		);
END_TYPE
