
PROGRAM _INIT

	// initialize the timer parameters
	TONRper_0.PT	:= T#5s;
	TONRper_0.pET	:= ADR(myTime); // variable myTime is retained and added to CPU.per
	 
END_PROGRAM

PROGRAM _CYCLIC

	// example timer logic
	TONRper_0.IN	:= countUpCmd AND NOT(pauseTimer);
	
	// call the timer FB
	TONRper_0();
	 
END_PROGRAM


